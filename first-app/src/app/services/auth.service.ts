import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { map, Observable, take, tap } from 'rxjs'
import { compareSync } from 'bcryptjs';

interface UserRegistrationType {
  username: string;
  password: string;
  hobbies: Array<{ name: string, frequency: string }>;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedIn: boolean | null = null;
  private token: string = "";

  constructor(
    private http: HttpClient,
    private router: Router) { }

  onUserRegister(user: UserRegistrationType) {
    this.http.post("http://localhost:3000/admins", user, {
      headers: {
        "Content-Type": "application/json"
      }
    }).subscribe({
      next: (response) => { console.log("[RESPONSE]", response) },
      error: err => console.log(err)
    })
  }

  onUserLogin(username: string, password: string) {
    this.http.get("http://localhost:3000/admins?username=" + username)
      .pipe(
        map(val => <Array<UserRegistrationType>>val),
        tap(val => console.log("TAP : ", val))
      )
      .subscribe({
        next: (response) => {
          if (response.length > 0) {
            const isMatch = compareSync(password, response[0].password)
            if (isMatch) {
              this.isLoggedIn = true;
              this.router.navigate(["/users"])
              this.token = "Some random token value";
              return console.log("Authenticated User")
            }
            return console.log("Bad Credentials")
          }
          console.error(new Error("Username NOT Found - " + username))
        },
        error: err => console.log(err)
      })
  }

  isUserAuthenticated() {
    return this.isLoggedIn !== null;
  }

  onUserLogout() {
    this.isLoggedIn = null;
    this.router.navigate(["/login"])
  }

  getUserToken() {
    return this.token;
  }

}
