import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as env from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CounterService {

  counter = 0;

  constructor(private http: HttpClient) { }

  getData() {
    this.http.get(env.environment.git_api)
  }
}
