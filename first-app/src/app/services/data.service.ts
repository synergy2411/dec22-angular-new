import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser } from '../model/user.interface';
import { Observable } from 'rxjs';

@Injectable({ providedIn: "root" })
export class DataService {

  constructor(private http: HttpClient) { }

  getUserData(): Observable<Array<IUser>> {
    return this.http.get<Array<IUser>>("http://localhost:3000/userdata")
  }

}
