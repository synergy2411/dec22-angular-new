import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IPost } from '../model/post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private baseURL = "http://localhost:3000/posts";

  // subject = new Subject<IPost>();

  constructor(private http: HttpClient) { }

  getPosts(): Observable<IPost[]> {
    return this.http.get<IPost[]>(this.baseURL)
  }

  getPost(postId: string): Observable<IPost> {
    return this.http.get<IPost>(`${this.baseURL}/${postId}`)
  }

  addPost(post: IPost) {
    return this.http.post(this.baseURL, post, {
      headers: {
        "Content-Type": "application/json"
      }
    })
  }

  deletePost(postId: string) {
    return this.http.delete(`${this.baseURL}/${postId}`)
  }
}
