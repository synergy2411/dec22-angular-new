import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { APP_ROUTES } from './app.routes';
import { CounterTwoComponent } from './components/counter-two/counter-two.component';
import { CounterComponent } from './components/counter/counter.component';
import { HeaderComponent } from './components/header/header.component';
import { LifeCycleDemoComponent } from './components/life-cycle-demo/life-cycle-demo.component';
import { LoginComponent } from './components/login/login.component';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { PipeDemoComponent } from './components/pipe-demo/pipe-demo.component';
import { PostItemComponent } from './components/posts/post-item/post-item.component';
import { PostsComponent } from './components/posts/posts.component';
import { RegisterComponent } from './components/register/register.component';
import { CommentFormComponent } from './components/users/comment-form/comment-form.component';
import { UserCommentComponent } from './components/users/user-comment/user-comment.component';
import { UserImgComponent } from './components/users/user-img/user-img.component';
import { UserInfoComponent } from './components/users/user-info/user-info.component';
import { UsersComponent } from './components/users/users.component';
import { HighlightDirective } from './directives/highlight.directive';
import { EmployeeModule } from './modules/employee/employee.module';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ReversePipe } from './pipes/reverse.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbUiComponent } from './components/ui/ngb-ui/ngb-ui.component';
import { AuthInterceptor } from './services/interceptor/auth.interceptor';

@NgModule({
  declarations: [       // Component, Directive, Pipe
    AppComponent,
    UsersComponent,
    UserInfoComponent,
    UserImgComponent,
    LifeCycleDemoComponent,
    UserCommentComponent,
    HighlightDirective,
    PipeDemoComponent,
    CountryCodePipe,
    ReversePipe,
    FilterPipe,
    CommentFormComponent,
    LoginComponent,
    RegisterComponent,
    CounterComponent,
    CounterTwoComponent,
    ObservableDemoComponent,
    HeaderComponent,
    PostsComponent,
    PostItemComponent,
    NgbUiComponent
  ],
  imports: [            // Module : Built-in / Custom
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES, { preloadingStrategy: PreloadAllModules }),
    EmployeeModule,
    NgbModule
  ],
  // providers: [CounterService],        // Services
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
