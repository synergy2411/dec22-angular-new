import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewEmpComponent } from './components/new-emp/new-emp.component';



@NgModule({
  declarations: [NewEmpComponent],
  imports: [
    CommonModule
  ],
  exports: [NewEmpComponent]
})
export class EmployeeModule { }
