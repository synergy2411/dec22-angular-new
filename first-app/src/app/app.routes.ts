import { Routes } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { ObservableDemoComponent } from "./components/observable-demo/observable-demo.component";
import { PipeDemoComponent } from "./components/pipe-demo/pipe-demo.component";
import { PostItemComponent } from "./components/posts/post-item/post-item.component";
import { PostsComponent } from "./components/posts/posts.component";
import { RegisterComponent } from "./components/register/register.component";
import { UsersComponent } from "./components/users/users.component";
import { LoginGuard } from "./services/guard/login.guard";

export const APP_ROUTES: Routes = [
  {
    path: "",                             // http://localhost:4200
    redirectTo: "login",
    pathMatch: "full"
  }, {
    path: "login",                             // http://localhost:4200/login
    component: LoginComponent
  }, {
    path: "register",                             // http://localhost:4200/register
    component: RegisterComponent
  }, {
    path: "pipe-demo",                              // http://localhost:4200/pipe-demo
    component: PipeDemoComponent
  }, {
    path: "users",                             // http://localhost:4200/users
    component: UsersComponent,
    canActivate: [LoginGuard]
  }, {
    path: "posts",
    component: PostsComponent
    // children : [
    //   // { path : ":postId", component : PostItemComponent}
    // ]
  }, {
    path: "posts/:postId",              // http://localhost:4200/posts/:postId
    component: PostItemComponent
  }, {
    path: "lazy",
    loadChildren: () => import("./modules/lazy/lazy.module").then(m => m.LazyModule)
  }, {
    path: "observable-demo",
    component: ObservableDemoComponent
  }, {
    path: "**",                               // http://localhost:4200/doesNotMatch
    redirectTo: "login",
    pathMatch: "full"
  }
]
