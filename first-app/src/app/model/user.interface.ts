import { IComment } from "./comment.interface";

export interface IUser {
  firstName: string;
  lastName: string;
  income: number;
  dob: Date;
  company: string;
  isWorking: boolean
  image: string;
  votes: number;
  comments: Array<IComment>
}
