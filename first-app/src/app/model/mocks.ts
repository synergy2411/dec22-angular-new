import { IUser } from "./user.interface";

export const USER_DATA: Array<IUser> = [
  {
    firstName: "bill",
    lastName: "gates",
    income: 50000,
    dob: new Date("Dec 21, 1965"),
    company: "Microsoft",
    isWorking: true,
    image: "https://pbs.twimg.com/profile_images/1564398871996174336/M-hffw5a_400x400.jpg",
    votes: 120,
    comments: [
      { stars: 4, body: "Like 👍 your work", email: "foo@test" },
      { stars: 3, body: "Awesome work", email: "bar@test" },
    ]
  },
  {
    firstName: "elon",
    lastName: "musk",
    income: 40000,
    dob: new Date("Jan 2, 1965"),
    company: "Amazon",
    isWorking: true,
    image: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Elon_Musk_2015.jpg/408px-Elon_Musk_2015.jpg",
    votes: 180,
    comments: [
      { stars: 5, body: "Luv ♥ your work", email: "foo@test" }
    ]
  }, {
    firstName: "steve",
    lastName: "jobs",
    income: 0,
    dob: new Date("Aug 14, 1965"),
    company: "Apple",
    isWorking: false,
    image: "https://thumbs.dreamstime.com/b/steve-jobs-portrait-illustration-manually-painted-capturing-apple-genius-56617906.jpg",
    votes: 210,
    comments: []
  }
]
