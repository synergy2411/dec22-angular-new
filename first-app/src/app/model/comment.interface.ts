export interface IComment {
  stars: number;
  body: string;
  email: string;
}
