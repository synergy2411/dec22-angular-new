import { Pipe, PipeTransform } from '@angular/core';

interface ITodo {
  label: string;
  status: string;
}

@Pipe({
  name: 'filter',
  pure: true
})
export class FilterPipe implements PipeTransform {

  transform(todoColl: Array<ITodo>, filteredStatus: string): Array<ITodo> {
    console.log("[TRANSFORM]");
    if (filteredStatus === undefined) {
      return todoColl
    }
    let resultArray = [];
    for (let todo of todoColl) {
      if (todo.status === filteredStatus) {
        resultArray.push(todo)
      }
    }
    return resultArray;
  }

}

// create a pipe which will sort the todoColl
