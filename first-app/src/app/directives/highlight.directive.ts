import { Directive, ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @Input("favColor") favColor!: string;

  @HostBinding("style.backgroundColor") bgColor!: string;
  @HostBinding("style.color") color!: string;

  @HostListener("mouseleave")
  onMouseLeave() {
    this.bgColor = "transparent"
    this.color = "#444"
    // this.elRef.nativeElement.style.backgroundColor = "#fff";
    // this.elRef.nativeElement.style.color = "#444"
    // this.elRef.nativeElement.style.boxShadow = "none"
  }

  @HostListener("mouseenter")
  onMouseEnter() {
    // this.bgColor = "goldenrod"
    this.bgColor = this.favColor;
    this.color = "#fff"
    // this.elRef.nativeElement.style.backgroundColor = "#ff45ff";
    // this.elRef.nativeElement.style.color = "#fff"
    // this.elRef.nativeElement.style.boxShadow = "0 0 25px 10px lightgrey"
  }

  constructor(private elRef: ElementRef) {
    // console.log(this.elRef.nativeElement)


  }

}

// <div appHighlight></div>


// <input type="text" name="username" />


// in css
// [name="username"]{ background : "grey" }
