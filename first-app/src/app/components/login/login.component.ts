import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  username = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  password = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    this.hasExclamation
  ]);
  loginForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.loginForm = this.fb.group({
      username: this.username,
      password: this.password
    })
  }

  onLogin() {
    console.log(this.loginForm);
    const { username, password } = this.loginForm.value;
    this.authService.onUserLogin(username, password)
  }

  // Custom Validator
  hasExclamation(control: AbstractControl) {
    const isExclamationAvailable = control.value.indexOf("!") >= 0
    return isExclamationAvailable ? null : { needExclamation: true }
  }
}
