import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
  ContentChild
} from '@angular/core';

@Component({
  selector: 'app-life-cycle-demo',
  templateUrl: './life-cycle-demo.component.html',
  styleUrls: ['./life-cycle-demo.component.css']
})
export class LifeCycleDemoComponent
  implements OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy {

  @Input("title") title: string = '';

  @ViewChild('txtUsername') txtUsername!: ElementRef;
  // @ViewChildren()    -> Returns the list of matching elements

  @ContentChild("headerInfo") header!: ElementRef;
  // @ContentChildren()   -> Returns the list of matching elements

  constructor() {
    console.log("CONSTRUCTOR")
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnChanges", changes)
  }
  ngDoCheck(): void {
    console.log("ngDoCheck")
  }
  ngAfterContentInit(): void {
    console.log("ngAfterContentInit")
    console.log("Content Child : ", this.header.nativeElement)
  }
  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked")
  }
  ngAfterViewInit(): void {
    console.log("ngAfterViewInit")
    console.log("Txt Username : ", this.txtUsername.nativeElement)
    console.log("Content Child in View INIT: ", this.header.nativeElement)
    this.txtUsername.nativeElement.value = 'Foo Bar'
    this.txtUsername.nativeElement.style.backgroundColor = 'green';
  }
  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked")
  }
  ngOnDestroy(): void {
    console.log("ngOnDestroy")
  }

  ngOnInit(): void {
    console.log("ngOnInit", this.title);
    // console.log("Content Child : ", this.header.nativeElement)
    // console.log("Txt Username : ", this.txtUsername.nativeElement)     // undefined
  }

}
