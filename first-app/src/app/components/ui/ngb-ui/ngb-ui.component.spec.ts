import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbUiComponent } from './ngb-ui.component';

describe('NgbUiComponent', () => {
  let component: NgbUiComponent;
  let fixture: ComponentFixture<NgbUiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgbUiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NgbUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
