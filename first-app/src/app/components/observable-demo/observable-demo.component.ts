import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-observable-demo',
  templateUrl: './observable-demo.component.html',
  styleUrls: ['./observable-demo.component.css']
})
export class ObservableDemoComponent implements OnInit, AfterViewInit {

  avatarUrl!: string;

  sub$!: Subscription;
  @ViewChild("search") serachTerm!: ElementRef;

  ngAfterViewInit() {

    // FORKJOIN
    // forkJoin({
    //   user: ajax.getJSON("https://api.github.com/users/synergy2411"),
    //   repos: ajax.getJSON("https://api.github.com/users/synergy2411/repos")
    // }).subscribe(console.log)


    // FLATTENING OPERATORS
    // const interval$ = interval(1000)
    // const click$ = fromEvent(document, "click")
    // click$.pipe(
    // mergeMap(() => interval$)
    // concatMap(() => interval$.pipe(take(3)))
    // switchMap(() => interval$.pipe(take(4)))
    //   exhaustMap(() => interval$.pipe(take(3)))
    // ).subscribe(console.log)



    // TYPE AHEAD SUGGESTION
    // const txtInput = this.serachTerm.nativeElement;
    // const source$ = fromEvent(txtInput, "keyup");
    // source$.pipe(
    //   debounceTime(1000),
    //   switchMap((event: any) => {
    //     const term = event.target.value
    //     return ajax.getJSON(`https://api.github.com/users/${term}`)
    //   })
    // ).subscribe((response: any) => {
    //   this.avatarUrl = response['avatar_url']
    // })
  }











  ngOnInit(): void {

    // FROM Collection
    // const numbers$ = from([101, 103, 105, 108])
    // numbers$.pipe(
    //   filter(val => val > 105),
    //   map(val => val * 10)
    // ).subscribe(console.log)


    // FROM EVENT
    // const click$ = fromEvent(document, "click")
    // click$.pipe(
    //   map(event => event.target)
    // ).subscribe(console.log)


    // INTERVAL
    // const interval$ = interval(1000);
    // interval$.pipe(
    //   take(5)
    // ).subscribe(console.log);
  }

  obs$ = new Observable((observer) => {
    setTimeout(() => { observer.next("First Package") }, 1000)
    setTimeout(() => { observer.next("second Package"); }, 3000)
    setTimeout(() => { observer.next("third Package"); }, 5000)
    // setTimeout(() => { observer.error(new Error("Something went wrong here")) }, 6000)
    setTimeout(() => { observer.next("fourth Package"); }, 7000)
    setTimeout(() => { observer.complete() }, 8000)

  })




  subject = new Subject()

  generateSub() {
    this.subject.next(Math.round(Math.random() * 100))
  }
  subscribeSub() {
    this.subject.subscribe(data => console.log(data))
  }

  onSubscribe() {
    this.sub$ = this.obs$.pipe(
      map(val => "Transformed " + val)
    ).subscribe({
      next: (data) => {
        console.log("SUB : ", data)
      },
      error: (err) => {
        console.error(err)
      },
      complete: () => console.log("COMPLETED")
    })
  }

  onUnsubscribe() {
    this.sub$.unsubscribe();
  }

}
