import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IPost } from 'src/app/model/post.interface';
import { PostService } from 'src/app/services/post.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit, OnDestroy {

  postCollection!: IPost[];
  sub$!: Subscription;

  constructor(
    private postService: PostService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.sub$ = this.postService.getPosts()
      .subscribe(posts => this.postCollection = posts)
  }

  ngOnDestroy(): void {
    this.sub$.unsubscribe();
  }

  onPostSelect(post: IPost) {
    this.router.navigate([`/posts/${post.id}`])         // http://localhost:4200/posts/p001
  }

}
