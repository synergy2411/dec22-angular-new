import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPost } from 'src/app/model/post.interface';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent implements OnInit {

  post: IPost = { title: "", body: "", id: "" };

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const { postId } = params;
      this.postService.getPost(postId)
        .subscribe(post => this.post = post)
    })
  }

  onDelete() {
    this.postService.deletePost(this.post.id)
      .subscribe(response => {
        alert("ITEM DELETED")
        this.router.navigate(["/posts"])
      })
  }
}
