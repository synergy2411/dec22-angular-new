import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-demo',
  templateUrl: './pipe-demo.component.html',
  styleUrls: ['./pipe-demo.component.css']
})
export class PipeDemoComponent implements OnInit {

  promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("First Package")
    }, 3000)
  })

  contactNumber = 987654321;

  helloStr = "Hello World";

  filteredStatus!: string;

  todoColl = [
    { label: "planting", status: "pending" },
    { label: "shopping", status: "completed" },
    { label: "grocery", status: "pending" },
    { label: "insurance", status: "completed" },
  ]

  onAddNew() {
    this.todoColl.push({ label: "New Item", status: "pending" })
    console.log(this.todoColl.length);
  }


  constructor() { }

  ngOnInit(): void {
  }

}
