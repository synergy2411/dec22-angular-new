import { Component, OnInit } from '@angular/core';
import { CounterService } from 'src/app/services/counter.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  constructor(public cs: CounterService, private ds: DataService) { }

  increase() {
    this.cs.counter++;
    // this.ds.getCounter()
  }

  ngOnInit(): void {
  }

}
