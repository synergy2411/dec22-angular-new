import { Component, Input, OnInit } from '@angular/core';
import { IComment } from 'src/app/model/comment.interface';
import { IUser } from 'src/app/model/user.interface';

@Component({
  selector: 'app-user-comment',
  templateUrl: './user-comment.component.html',
  styleUrls: ['./user-comment.component.css']
})
export class UserCommentComponent implements OnInit {

  @Input("comments") comments!: Array<IComment>;

  tab: number = 0;

  myDynamicClasses = ['list-group-item', 'my-1', 'my-class'];

  constructor() { }

  ngOnInit(): void {
  }

  onAddNewComment(comment: IComment) {
    this.comments.push(comment);
    this.tab = 1
  }

}
