
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUser } from 'src/app/model/user.interface';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class UsersComponent implements OnInit {

  userCollection!: Array<IUser>;

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(query => console.log("QUERY : ", query))
    this.dataService.getUserData()
      .subscribe(users => this.userCollection = users)
  }

  onMoreInfo(user: IUser) {
    alert(`Mr. ${user.lastName} is working with ${user.company}`)
  }

  // changeVote(element: HTMLInputElement) {
  //   this.user.votes = Number(element.value)
  // }

}
