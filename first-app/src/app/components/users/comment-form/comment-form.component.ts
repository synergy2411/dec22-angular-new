import { Component, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IComment } from 'src/app/model/comment.interface';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent {

  @Output() addCommentEvent = new EventEmitter<IComment>()

  onSubmit(form: NgForm) {
    const { stars, body, email } = form.value;
    let newComment: IComment = { stars, body, email }
    this.addCommentEvent.emit(newComment);
  }

}
