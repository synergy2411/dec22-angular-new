import { Component, OnInit } from '@angular/core';
import { hashSync } from 'bcryptjs';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  registerForm!: FormGroup;

  courses = new FormArray<FormGroup>([]);

  addCourse() {
    const group = this.fb.group({
      name: "",
      duration: ""
    })
    this.courses.push(group)
  }

  constructor(
    private fb: FormBuilder,
    private authService: AuthService
  ) {
    this.registerForm = this.fb.group({
      uname: new FormControl(),
      password: new FormControl(),
      hobbies: this.fb.array([
        new FormGroup({
          name: new FormControl(),
          frequency: new FormControl()
        })
      ])
    })
  }

  get hobbies(): FormArray {
    return this.registerForm.get("hobbies") as FormArray;
  }

  addHobby() {
    const group = this.fb.group({
      name: "",
      frequency: ""
    })
    this.hobbies.push(group)
  }

  onRegister() {
    console.log(this.registerForm.value);
    let { uname, password, hobbies } = this.registerForm.value;
    const hashedPassword = hashSync(password, 10)
    this.authService.onUserRegister({ username: uname, password: hashedPassword, hobbies })
  }
}
