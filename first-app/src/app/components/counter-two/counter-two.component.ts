import { Component, OnInit } from '@angular/core';
import { CounterService } from 'src/app/services/counter.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-counter-two',
  templateUrl: './counter-two.component.html',
  styleUrls: ['./counter-two.component.css'],
  // providers: [CounterService]
})
export class CounterTwoComponent implements OnInit {

  constructor(public cs: CounterService, private ds: DataService) { }

  decrease() {
    this.cs.counter--;
    // this.ds.getCounter();
  }

  ngOnInit(): void {
  }

}
