Tea Break : 11:00 (15 Mins)
Lunch Break : 1:00 (45 Mins)
Tea Break : 3:30 (15 Mins)

# What is Angular ?

- to create mobile and web apps
- Framework for frontend apps
- creating SPA
- structured architecture
- 2 way data binding
- Components based
- MVC pattern
- Remote Server call / AJAX Calls
- Animation
- State Management

# Other Libraries and Frameworks

- jQuery : DOM manipulation, Ajax Calls, Animation
- React : Render the UI quickly and efficiently
- Vue : Template driven approach
- Knockout : MVVM architecture; 2 way data binding
- Backbone : MVC Pattern
- Stencil : Custom Components
- Polymer : Custom Elements
- D3 : for charting
- \*Ember : Heavy framework; Frequent change in internal API's
- AngularJS (2011): v1; Library
- Angular (2014): Latest Version; Framework

# JavaScript Types -

- Primitive Types : number, string, boolean
- Reference Types : object, array, date, function
- Other Types in TS -
  > any
  > enum
  > void
  > never
  > unknown
  > tuple
  > null
  > undefined
- Custom Type : "type"
- Link - typescriptlang.org

# Angular CLI Tools

- npm install @angular/cli@14 -g
- ng version
- ng new first-app
- cd first-app
- ng serve OR npm start

# Angular Building Blocks -

1. Component : ES6 Class + @Component()
2. Module : ES6 Class + @NgModule()
3. Directives : ES6 Class + @Directive()
4. Pipes : ES6 Class + @Pipe()
5. Services : ES6 Class + @Injectable() -> Platform Level | Module Level | Component / Directive Level

# to install bootstrap -

> npm i bootstrap@4

> ng g c components/users
> ng g

# Component Types

- Smart / Container / Parent : contains some business logic / model associated (UsersComponent)
- Dump / Presentational / Child : receives data from parent and display them on UI (UserInfo/UserImage)

## View Encapsulation

- Emulated (default)
  > Local CSS will take preference; Global CSS will apply when local CSS does not have rule
- None (Dangerous)
  > Local CSS will affect the other part of your app
- ShadowDOM
  > Only Local CSS will apply, no global CSS rule will apply

# Component Life Cycle Methods

- ngOnChange
- ngOnInit
- ngDoCheck
- ngAfterContentInit > @ContentChild() : used for projected content
- ngAfterContentChecked
- ngAfterViewInit > @ViewChild() : used to access component template
- ngAfterViewChecked
- ngOnDestroy

# Directives

- Structural : able to change the DOM Layout, *ngIf, *ngFor, \*ngSwitch
- Attribute : able to change the appearance of DOM element; ngStyle, ngClass
- Custom Directive : ElmentRef, @HostListener, @HostBinding

# Pipes : Formatting the model on UI

- Impure Change -> Reference is NOT Changed
  let fruits = ["apple", "banana"];
  fruits.push("kiwi");

- Pure Change
  let fruits = ["apple", "banana"];
  fruits = ["apple", "banana", "kiwi]; // creating new ref in memory

# Pure Pipe : will run on Pure Changes :

- Efficient and fast

# Impure Pipe: will run on both type of changes :

- inefficient :
- should not used with array having huge number of elements

# Forms

- Template Driven
  > HTML5 Validations
  > Validation logic will reside in template
- Model / Reactive Form

# Form & Form Element - State and Classes

- ngValid / ngInvalid
- ngPristine / ngDirty
- ngTouched / ngUntouched

hobbies : [ { name : "swmming", frequency : 2 } ]

FormControl : individual form element
FormGroup : combination of form Elements;
FormArray : can contain FormControl; FormGroup

formArray = [new FormControl(), new FormControl()];
formArray = [ {
new FormControl(), new FormControl()
}, {
new FormControl(), new FormControl()
} ]

# Dependency Injection

- Root Module : Singleton instance available app-wide
- Root Component : Singleton instance available to self and child component
- Other Component : Singleton instance available to self and child component

- If two service are registered on the same level, they share the same singleton instance

# Difference between Promises and Observable -

- Observables : A stream on which data arrive at certain time interval

  > keeps an eye on Data Source
  > give the series of data (events)
  > are lazily executed; does not execute until subscribe
  > are both Synchronous and Asynchronous
  > are cancelable
  > support of operators

- Promises

  > one shot data
  > are eagerly executed
  > are always asynchronous
  > are not cancelable

# Observable Operators -

## flatten the inner observable

- mergeMap : merge all the emission of inner observables
- concatMap : let it complete the first observable and then start the new one
- switchMap : stop the running observable and switch to new one
- exhaustMap : ignore the emission of new observables, if the existing observable is still running
- forkJoin
- debounce

# Subject : Observable as well as Observer

- BehaviourSubject : contains the seed value
- ReplaySubject : replay the subscirption or number of time
- AsyncSubject : subscribe the last emitted value

- For Observable API / Operators

  > reactivex.io
  > rxjs.dev

  # HttpClient : Remote Server Call

# Routing Terminologies -

- Routes : configure the components based upon the URL
- RouterModule : supply the route config in app
- RouterOutlet : provide space on the UI to load component template
- RouteLink : prevent the page to reload
- Router : to navigate the user programaticaly

# to include ng-bootstrap

> ng add @ng-bootstrap/ng-bootstrap
> install the extension > NgbBootstrap Snippets

# Angular Interview Questions

- Difference between Component and Directive ?
- Explain the Component life cycle methods in sequence.
- Difference between Structural and Attribute Directive. Examplify them?
- Difference between Template-driven and Model-driven forms. In which situation to use Reactive/Template Forms?
- Differnce between Pure & Impure pipes
- Explain the (DI) Hierarchical Injection
- Explain the "providedIn" property
- Use Case for canActivate guard
- "Banana in the box" syntax
- Explain various properties for view encapsulation

# View Encapsulation -

- Emulated (Default) : Global CSS rule will apply on component template if no Local CSS rule is available
- None : No Encapsulation, Local CSS rule will affect the other part of the App
- Shadow DOM / Native : full encapsulation; component will not be affected by external CSS Rule.

Comp A -> None (h2) h2 {color : "red" }
Comp B -> Emulated (h2)
Comp C -> ShadowDOM (h2)
